package com.lucher.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

/**
 * 用来显示dialog效果的类，程序的主类
 */
public class DialogActivity extends Activity implements Button.OnClickListener {
	/** Called when the activity is first created. */

	Button btnDialog1, btnDialog2, btnDialog3, btnDialog4, btnDialog5; // 显示五个dialog的按钮
	final int SHOW_DIALOG3 = 1, SHOW_DIALOG4 = 2, SHOW_DIALOG5 = 3;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		initViews();
	}

	/**
	 * 初始化组件
	 */
	private void initViews() {
		// TODO Auto-generated method stub
		btnDialog1 = (Button) this.findViewById(R.id.dialog1); // 获取按钮 下同
		btnDialog1.setOnClickListener(this); // 为按钮加上监听 下同
		btnDialog2 = (Button) this.findViewById(R.id.dialog2);
		btnDialog2.setOnClickListener(this);
		btnDialog3 = (Button) this.findViewById(R.id.dialog3);
		btnDialog3.setOnClickListener(this);
		btnDialog4 = (Button) this.findViewById(R.id.dialog4);
		btnDialog4.setOnClickListener(this);
		btnDialog5 = (Button) this.findViewById(R.id.dialog5);
		btnDialog5.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		System.out.println(v.getId());
		switch (v.getId()) {
		case R.id.dialog1:
			showDialog1();
			break;
		case R.id.dialog2:
			showDialog2();
			break;
		case R.id.dialog3:
			showDialog(SHOW_DIALOG3);
			break;
		case R.id.dialog4:
			showDialog(SHOW_DIALOG4);
			break;
		case R.id.dialog5:
			showDialog(SHOW_DIALOG5);
			break;
		}
	}

	/**
	 * activity用来显示dialog
	 */
	private void showDialog1() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, ShowDialog.class);
		startActivity(intent);
	}

	/**
	 * 代码布局显示dialog
	 */
	private void showDialog2() {
		// TODO Auto-generated method stub
		LayoutParams lp_main = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
		LinearLayout main = new LinearLayout(DialogActivity.this);
		main.setOrientation(LinearLayout.VERTICAL);
		main.setLayoutParams(lp_main);

		LayoutParams lp_name = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		lp_name.setMargins(15, 5, 2, 5);
		LinearLayout dia_male = new LinearLayout(DialogActivity.this);
		dia_male.setOrientation(LinearLayout.HORIZONTAL);
		TextView txt_male = new TextView(DialogActivity.this);
		txt_male.setTextSize(25);
		txt_male.setPadding(10, 0, 15, 0);
		txt_male.setText("这是dialog2");
		final EditText etxt_male = new EditText(DialogActivity.this);
		etxt_male.setWidth(180);
		etxt_male.setTextSize(25);
		dia_male.addView(txt_male);
		dia_male.addView(etxt_male);
		dia_male.setLayoutParams(lp_name);
		main.addView(dia_male);

		Builder dialog = new AlertDialog.Builder(DialogActivity.this)
				.setTitle("Dialog2").setIcon(android.R.drawable.ic_dialog_info)
				.setView(main).setPositiveButton("确定", null)
				.setNegativeButton("取消", null).setInverseBackgroundForced(true);
		dialog.show();
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle args) {
		// TODO Auto-generated method stub
		Dialog dialog = null;
		switch (id) {
		case SHOW_DIALOG3:
			Builder b = new AlertDialog.Builder(this);
			b.setIcon(R.drawable.ic_launcher);// 设置图标
			b.setTitle("Dialog3");// 设置标题
			b.setItems(R.array.searchbook,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// 这里加点击列表中的内容产生结果的代码
							switch (which) {
							case 0:
								//这里可以实现选中第一项后的方法
								break;
							case 1:
								//这里可以实现选中第二项后的方法
								break;
							}
						}
					});
			dialog = b.create();
			break;
		case SHOW_DIALOG4:
			dialog = new MyDialog(this);
			break;
		case SHOW_DIALOG5:
			b = new AlertDialog.Builder(this);
			b.setItems(null, null);
			dialog = b.create();
			break;
		}
		return dialog;
	}

	@Override
	protected void onPrepareDialog(int id, final Dialog dialog) {
		// TODO Auto-generated method stub
		switch (id) {
		case SHOW_DIALOG4:
			// 确定按钮
			Button bjhmcok = (Button) dialog.findViewById(R.id.bjhmcOk);
			// 取消按钮
			Button bjhmccancel = (Button) dialog.findViewById(R.id.bjhmcCancle);
			// 给取消按钮添加监听器
			bjhmcok.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) { // 关闭对话框
					//可以添加自己的处理方法
					dialog.cancel();
				}
			});
			bjhmccancel.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) { // 关闭对话框
					//可以添加自己的处理方法
					dialog.cancel();
				}
			});
			break;
		case SHOW_DIALOG5:
			LayoutParams lp_main = new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
			LinearLayout main = new LinearLayout(DialogActivity.this);
			main.setOrientation(LinearLayout.VERTICAL);
			main.setLayoutParams(lp_main);

			LayoutParams lp_name = new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			lp_name.setMargins(15, 5, 2, 5);
			LinearLayout dia_male = new LinearLayout(DialogActivity.this);
			dia_male.setOrientation(LinearLayout.HORIZONTAL);
			TextView txt_male = new TextView(DialogActivity.this);
			txt_male.setTextSize(25);
			txt_male.setPadding(10, 0, 15, 0);
			txt_male.setText("这是dialog5");
			final EditText etxt_male = new EditText(DialogActivity.this);
			etxt_male.setWidth(180);
			etxt_male.setTextSize(25);
			dia_male.addView(txt_male);
			dia_male.addView(etxt_male);
			dia_male.setLayoutParams(lp_name);
			main.addView(dia_male);
			main.setBackgroundResource(R.drawable.dialog);

			dialog.setContentView(main);
			break;
		}
	}

}